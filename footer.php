	
	<?php if (!get_template_part('templates/page-contato.php')) { _partial('_secao-cta_rodape'); } ?>
	<footer id="footer" class="mt-md-2 mt-lg-10">		

		<div class="container rubik text-white">

			<div class="row">

				<div class="col-lg-8 d-flex flex-column flex-md-row justify-content-md-between">

					<div class="end">						
						<?php 
							_p('p', get_field('rodape_endereco_titulo', 'option'), 'h4 rubik' );
							_p('p', get_field('endereco_info', 'option'), 'mb-4');
						?>
					</div>

					<div class="hora">
						<?php 
							_p('p', get_field('rodape_horarios_titulo', 'option'), 'h4 rubik' );
							_p('p', get_field('atendimento_info', 'option'), 'mb-4');
						?>
					</div>

					<div class="contatos">
						<?php 
							_p('p', get_field('rodape_contatos_titulo', 'option'), 'h4 rubik' );
							_p('p', get_field('contatos', 'option'), 'mb-4');						
						?>
					</div>

				</div>
				
				<div class="col-lg-4">
					<?php _partial('_social-links'); ?>
				</div>

				<div class="col-sm-12 text-md-center py-6">
					<?php 
						_p('p', get_field('rodape_vagas_titulo', 'option'), 'font-weight-light h4');
						_p('p', get_field('vagas_email', 'option'), 'text-gold my-1');
						_p('p', '© '. date('Y'));
					?>
				</div>

			</div>

		</div>		

	</footer>

	<a id="go-to-top" href="#top" title="<?php _e('Go to top', 'react'); ?>" class="d-flex align-items-center justify-content-center pointer">
		<span class="d-inline-block icon-slider-up"></span>
	</a>
	
	
	<?php /* */ ?>
	<div class="bg">  <!-- get the body size -->

		<div class="bg--grid"> <!-- create a relative element with the size of body -->
			
			<div class="bg--gradient--header"></div>

				<?php if (is_front_page()): ?>
					<div id="capacete" class="bg--default bg--capacete rocket-lazyload" data-bg="url(<?php images_url('bg-astronauta-rosto.jpg'); ?>)"></div>
					<div class="bg--default bg--planetinha rocket-lazyload" data-bg="url(<?php images_url('png/luz-planeta-2.png') ?>)"></div>
					<div id="satelite" class="bg--default bg--satelite rocket-lazyload" data-bg="url(<?php images_url('png/nave-1.png') ?>)"></div>
					<div id="astronauta-right" class="bg--default bg--astronauta-right rocket-lazyload" data-bg="url(<?php images_url('png/astronauta-caindo.png') ?>)"></div>
					<div id="astronauta-left" class="bg--default bg--astronauta-left rocket-lazyload" data-bg="url(<?php images_url('png/astronauta-em-pe.png') ?>)"></div>
				<?php endif; ?>
			<div id="rocket" class="bg--default bg--rocket mousemove rocket-lazyload" data-direction="-1" data-speed="45" data-bg="url(<?php images_url('png/nave.png') ?>)"></div>
			<div class="bg--clouds mousemove rocket-lazyload" data-direction="1" data-speed="30" data-bg="url(<?php images_url('nuvem-2.png') ?>)"></div>

		</div>
	</div>
	<?php /*
		<div id="controls">
			<button data-target="#header">header</button>		
			<button data-target="#main-content">content</button>		
			<button data-target=".secao.cta_rodape">CTA</button>		
			<button data-target="#footer">footer</button>		
			<button data-target="#header, #main-content, .secao.cta_rodape, #footer">all</button>		
		</div>
	*/ ?>
<?php wp_footer(); ?>

<script src="https://static.zenvia.com/embed/js/zenvia-chat.min.js"></script>
<script>
    var chat = new ZenviaChat('577c741ef53df02464cb12c7de1fad36')
        .embedded('button').build();		
		var lazyLoadInstance = new LazyLoad({
			elements_selector: "[data-src], [data-bg]"
		});
</script>

</body>
</html>
