<?php 

    global $wp_query;

    if (is_search() && isset($_GET['s']) && $_GET['s']) {
        echo '<div class="header-search mb-4">';

                $qtd = $wp_query->found_posts;

                if ($qtd > 0) {
                    _p('p', sprintf(__('Sua busca por %s retornou %s resultado(s)', 'react'), '<strong>'.get_search_query().'</strong>', $qtd));                    
                } else {
                    _p('p', sprintf(__('Nenhum resultado encontrado para %s. Tente outro termo.', 'react'), '<strong>'.get_search_query().'</strong>'));
                }

        echo '</div>';
    }

?>