<?php 
    $post_id = get_post_id();
    // Load data from current page, or get the defaults from options page    
    $exibir = get_field('exibir_cta', $post_id) ? get_field('exibir_cta', $post_id) : get_field('exibir_cta', 'option');
    if ($exibir === 'nao') return;
    
    $titulo = get_field('cta_titulo', $post_id) ? get_field('cta_titulo', $post_id) : get_field('cta_titulo', 'option');
    $subtitulo = get_field('cta_subtitulo', $post_id) ? get_field('cta_subtitulo', $post_id) : get_field('cta_subtitulo', 'option');
    $descricao = get_field('cta_descricao', $post_id) ? get_field('cta_descricao', $post_id) : get_field('cta_descricao', 'option');
    $link = get_field('cta_link', $post_id) ? get_field('cta_link', $post_id) : get_field('cta_link', 'option');    
    $titleLink = $link['title'] ? $link['title'] :  get_field('cta_link', 'option')['title'];
    
?>

<div class="secao cta_rodape py-10">
    <div class="container text-center">
        <?php 
            _p('p', $titulo, 'h1 rubik text-gold titulo font-weight-black');
            _p('p', $subtitulo, 'h1 rubik text-white subtitulo font-weight-light');
            _p('p', $descricao, 'rubik descricao h4 font-weight-light text-white');         
            _p('a:acf-link', $titleLink, [
                'class' => 'btn btn-lg btn-primary px-5 text-white rubik font-weight-bold mt-2 mb-0 mb-md-2', 'attr' => $link 
            ]);            
        ?>
    </div>
</div>