<?php   
  $logotipoMobile = get_field('mobile_logotipo', 'options', false);  
  $logotipoMobileData = $logotipoMobile ? wp_get_attachment($logotipoMobile) : false;
  $logotipoID = get_field('logotipo', 'options', false);
  $logotipoData = $logotipoID ? wp_get_attachment($logotipoID) : false;
?>
  <div class="row">
    <nav id="navmenu" class="navbar navbar-expand-lg navbar-light animation-close-toggle w-100 py-lg-0">
      <a class="navbar-brand pl-2 pl-lg-0 mr-md-8 mr-lg-2" href="<?php echo get_home_url(); ?>"> 
        
        <?php         
          // Se existir um $logotipoMobile cadastrado, altera as classes
          $logotipoCssClass = $logotipoMobile ? 'd-none d-md-inline-block' : 'd-inline-block';
          // Se estiver cadastrado o logotipo, exibe o mesmo. 
          // Caso contrário exibe o nome do site          
          $headerName = $logotipoID ? 
            '<img src="'.$logotipoData['src'].'" class="'.$logotipoCssClass.' img-fluid" alt="'.$logotipoData['alt'].'"/>' :
            get_bloginfo( 'name' );
          echo $headerName;

          // Se existir logo para mobile, exibe-o
          echo ($logotipoMobile ? '<img src="'. $logotipoMobileData['src'] .'" height="30" class="d-inline-block d-md-none img-fluid align-top" alt="'.$logotipoMobileData['alt'].'">' : '');

        ?>
        
      </a>
      
      <button class="navbar-fire collapsed" type="button" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span></span><span></span><span></span>
      </button>

        <?php 
          wp_nav_menu( array(         
            'theme_location'    => 'primary',
            'depth'             => 3,
            'container'         => 'div',
            'container_class'   => 'navbar-collapse pt-2 pt-lg-0',            
            'container_id'      => 'navbarNav',
            'menu_class'        => 'navbar-nav w-100 d-md-flex justify-content-md-between', // A classe mr-auto alinha o menu a direita
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker()
          )); 
        ?>          
    </nav><!-- /.navbar-collapse -->    
  </div>
  <style> #navbarNav::before{background-image: url(<?php echo $logotipoData['src']; ?>); }</style>