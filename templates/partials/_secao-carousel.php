<section class="secao <?php echo $secao['acf_fc_layout'] ?> my-10">
    <div class="container">
        <?php if ($secao['titulo']) : ?>
            <h2 class="text-uppercase mb-3 mb-md-5 line line--short text-gold font-weight-bold">                
                <span class="append-arrows clearfix">
                    <span class="content"><?php echo $secao['titulo']; ?></span>
                    <span class="arrows"></span>
                </span>
            </h2>
        <?php endif; ?>

        <?php if ($secao['items']) : ?>
            <div class="secao--carousel">
                <?php markup_carousel($secao['items'], ['layout' => $secao['layout']]) ?>    
            </div>
        <?php endif; ?>
    </div>
</section>
