<?php 
    global $post;
    
    $post_id = apply_filters( 'wpml_object_id', get_post_ID(), 'post' );    
    $bg_header = get_field('background_header', $post_id);
    $layoutHeader = sanitize_title(get_field('layout_header', $post_id));
    $extraClass = 'pageheader--'.$layoutHeader;
    if ($bg_header) { $extraClass .= ' pageheader--custombg'; }
    
?>
<header class="pageheader text-center mb-8 text-white d-flex align-items-center <?php echo $extraClass; ?> "> 
    
    <div class="container">
        
        <?php         

            if ( function_exists('yoast_breadcrumb') && 
                    ( is_archive() || is_single() && $post->post_type == 'post' ) && 
                    !is_tax('areas')
                ) { 
                yoast_breadcrumb('<p id="breadcrumbs">','</p>'); 
            }
            $position = get_field('posicao_do_titulo', $post_id);
            // Positions
            $headlineTag = $position === 'headline' ? 'h1' : 'p';
            $titleTag = $position === 'headline' ? 'p' : 'h1';
            
            // Contents
            $headlineContent = $position === 'headline' ? get_the_title($post_id) : get_field('headline', $post_id);
            $titleContent = $position === 'headline' ? get_field('titulo', $post_id) : get_the_title($post_id);
            
            // Não exibe a headline no single-post
            // if (!is_single() && !is_post_type_archive('post') && !is_post_type_archive('servico')) {
                _p($headlineTag, $headlineContent, 'h4 mb-0');                
            // }
            _p($titleTag, $titleContent, 'title font-weight-light text-uppercase');

            if ($bg_header) {
                echo '<style>.pageheader:after{background-image: linear-gradient( 0, rgb(255,132,0) 0%, rgba(183,26,37,.7) 100%), url('.$bg_header.');}</style>';
            }
        ?>

    </div>

</header>