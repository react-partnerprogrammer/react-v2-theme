<?php if( have_rows('sociais_info','option') ): ?>
	<ul class="redes-sociais my-2 list-inline d-flex justify-content-center justify-content-lg-end flex-wrap">
		<?php  while ( have_rows('sociais_info','option') ) : the_row(); ?>
			<li class="list-inline-item">
				<a href="<?php the_sub_field('url_info'); ?>" class="d-flex align-items-center ljoptimizer justify-content-center h-100" rel="nofollow noopener" target="_blank">
					<i class="d-inline-block icon-<?php the_sub_field('icone_info'); ?>"></i>
				</a>
			</li>
		<?php endwhile; ?>
	</ul>
<?php endif; ?>


