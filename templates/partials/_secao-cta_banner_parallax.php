<section class="secao <?php echo $secao['acf_fc_layout'] ?> my-6">
    <div class="container pos-relative">
        <div class="row">
            <div class="col-lg-8 transition-parallax">
                <div class="bg-box py-4 px-2 pl-md-8 pr-md-10 pt-md-7 pb-md-2 bg-cover" style="background-image:url(<?php echo $secao['background']; ?>)">
                    <h2 class="line font-weight-light"><?php echo $secao['titulo']; ?></h2>
                    <div class="open-sans">
                        <?php echo $secao['descricao']; ?>
                    </div>
                </div>
            </div>
            <?php 
                if ($secao['banner']) {                                
                    echo '<div class="d-none d-lg-flex col-lg-5 banner align-items-center h-100">';
                        _p('img:acf-image', $secao['banner'] , [ 'class' => 'img-fluid mx-auto d-block']);
                    echo '</div>';
                        //     '<a class="transition-parallax" href="'.$secao['link']['url'].'" title="'.$secao['link']['title'].'">',
                        //         '<img src="'.$secao['banner'].'"class="img-fluid mx-auto">',
                        //     '</a>',
                        // '</div>'; 
                } 
            ?>
        </div>
    </div>
</section>