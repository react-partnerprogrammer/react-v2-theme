<?php // echo '<pre>'.print_r($secao,1). '</pre>'; ?>
<section class="secao <?php echo $secao['acf_fc_layout'] ?> my-6">
	<div class="container">
        <?php if ($secao['titulo']) : ?>
            <h2 class="text-uppercase mb-3 line line--short text-gold font-weight-bold">
                <span class="content"><?php echo $secao['titulo']; ?></span>
                <span class="append-arrows"></span>
            </h2>
        <?php endif;
        
            _p('p', $secao['subtitulo'], [ 'class' => 'rubik h4 font-weight-normal text-malul']);

			$cpts = new WP_Query( [
                'post_type' => 'post', 
                'posts_per_page' => $secao['quantidade_de_posts_a_exibir'] 
                ] ); 
                if ($cpts->have_posts()) {
                    echo '<div class="secao--blog row mx-0">';
                        while ( $cpts->have_posts() ) : $cpts->the_post(); 
                            _partial('_content-post');
                        endwhile; 			
                    echo '</div>';
                }
			wp_reset_postdata();
		?>

		<div class="text-center">
            <a href="<?php echo home_url() ?>/blog" class="btn btn-outline-gold mt-5 d-inline-block">
                <?php _e('VER MAIS', 'react'); ?>
            </a>
		</div>
	</div>
</section>
