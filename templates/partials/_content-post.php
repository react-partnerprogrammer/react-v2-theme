<?php 
	// global $post;	
	// setup_postdata( $post ); 
	// $terms = wp_get_post_terms($post->ID, 'areas',['fields' => 'names']);	
	$thumbSize = is_front_page() ? 'thumb-blog-home' : 'thumb-blog';
?>
	<article <?php post_class('col-lg-4 px-0 my-1') ?>>
		<a href="<?php the_permalink(); ?>" class="d-flex align-items-end h-100 p-3 bg-cover" <?php echo thumbnail_bg() ?> >
			<?php 
				// if (has_post_thumbnail()) {
				// 	the_post_thumbnail($thumbSize, [ 
				// 		'class' => 'd-flex align-self-start mb-3 mr-3 img-fluid',
				// 		'alt' => get_the_title()
				// 	] );
				// }
			?>
			<div class="info pt-10">
				<p class="author text-gold m-0 rubik font-weight-medium mb-1"><?php _e('Por ', 'react'); the_author(); ?></p>
				<h2 class="text-white h4 rubik m-0"><?php the_title(); ?></h2>				
				<p class="category text-gold text-uppercase m-0"><?php echo my_list_categories(); ?></p>
			</div>
		</a>
	</article>