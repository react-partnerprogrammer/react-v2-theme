
<?php global $post; ?>
	
	<?php _partial('_header') ?>
	
	
	<div class="solucoes">
		
		<?php 
			$extraStyles = '';
			$count = 0;
			while (have_posts()) : the_post();
				$extraStyles .= '#servico-' . get_the_ID() . ' .wrapper:before { background-image: url('.get_field('background').'); } ';
				$btnClass = $count % 2 == 0 ? 'btn-primary text-white' : 'btn-outline-primary';
				echo '<article id="servico-'.get_the_ID().'" class="solucao bg-cover">';

					echo '<div class="wrapper d-block py-10" >';
						echo '<div class="container text-left">';								
							_p('h2', get_the_title(), 'rubik text-primary h2');
							_p('p', get_field('chamada'), 'chamada rubik font-weight-light h4');
							// _p('span', get_field('chamada_cta') . ' >>', 'd-inline-block mt-2 text-uppercase px-3 py-1 h6 btn ' . $btnClass);	
							_p('a', get_field('chamada_cta') . ' >>', [
								'attr' => [
									'class' => 'd-inline-block mt-2 text-uppercase px-3 py-1 h6 btn ' . $btnClass,
									'href' => get_permalink(),
									'title' => get_the_title(),
								]
							]);	
						echo '</div>';
					echo '</div>';

				echo '</article>';
				$count++;
			endwhile;
		?>
	</div>
	

	<style type="text/css"> <?php echo $extraStyles; ?> </style>
	