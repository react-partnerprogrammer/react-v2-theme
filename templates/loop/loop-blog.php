<?php 
	
	_partial('_header');

		echo '<div class="container my-8">';

			echo '<div class="row">';
				echo '<div class="col-12 d-md-none mb-4" style="margin-top: -2rem;">';
					get_search_form();
				echo '</div>';
				echo '<div class="col-lg-9">';
					
					_partial('_header-search');

					if (have_posts()): 

						while (have_posts()) : the_post();
						
							_loop('content-post');
						
						endwhile; 
				
						if (function_exists('wp_pagenavi')) { wp_pagenavi(); };
				
					endif; 

				echo '</div>';
				
				get_sidebar();

			echo '</div>';

		echo '</div>';

	wp_reset_query(); 
?>