<article <?php post_class('col-xl-4 px-0 my-1') ?>>
    <a href="<?php the_permalink(); ?>" class="d-flex align-items-start h-100 p-6" >			
        <div class="info">   
            <?php _p('img:acf-image', get_field('icone_relacionados'), 'mb-1') ?>         
            <h2 class="text-white h4 rubik mb-3"><?php the_title(); ?></h2>				
            <?php _p('p', get_field('chamada'), 'chamada open-sans h6 font-weight-light '); ?>
        </div>
    </a>
</article>