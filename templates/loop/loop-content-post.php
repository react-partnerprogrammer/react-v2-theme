<?php 
	global $post;	
	setup_postdata( $post ); 
	// $terms = wp_get_post_terms($post->ID, 'areas',['fields' => 'names']);	
	$thumbSize = is_front_page() ? 'thumb-blog-home' : 'thumb-blog';
?>
	<article class="mb-4 content-post">
		<a href="<?php the_permalink(); ?>" class="media flex-row">
			<?php 
				if (has_post_thumbnail()) {
					the_post_thumbnail($thumbSize, [ 
						'class' => 'd-flex align-self-start mb-3 mr-3 img-fluid',
						'alt' => get_the_title()
					] );
				}
			?>
			<div class="media-body">
                <?php 
					_p('h2', get_the_title(), 'h4 rubik');
					echo '<span class="excerpt">';
						the_excerpt();
					echo '</span>';
                ?>
				<p class="rubik h6 text-gold"><?php echo __('Por', 'react') . ' '; the_author(); ?> - <?php the_time( get_option( 'date_format' ) ); ?></p>
			</div>
		</a>
	</article>