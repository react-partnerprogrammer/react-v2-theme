<?php while (have_posts()) : the_post(); ?>
	
	<?php _partial('_header') ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class('container'); ?> >
	    
		<div class="col-lg-10 col-xl-8 mx-auto">
			<?php 
				global $post;
				$obj = get_post_type_object( $post->post_type );	
						
				// echo '<pre>'.print_r($post,1). '</pre>';
				// die();
				the_content();

				echo '<div class="my-6">'.do_shortcode('[easy-social-share]').'</div>';						

				// echo '<hr class="my-6" />';								
			?>
		</div>
	</div>

	<?php 
		// $page_servicos_id = 10;
		// $bg = get_field('background_servicos_relacionados', $page_servicos_id);
		// echo '<div class="py-10 bg-cover" style="background-image:url('.$bg.')">';
		
		// 	echo '<div class="container">';
		// 		pp_related([
		// 			'class' => 'mb-8 related--' . $post->post_type,
		// 			'title' => get_field('titulo_servicos_relacionados', $page_servicos_id),
		// 			'loop' => 'related-' . $post->post_type,
		// 			'posts_per_page' => 3,
		// 			'titleClass' => 'line line--short',
		// 			'descricao' => get_field('descricao_servicos_relacionados', $page_servicos_id),
		// 		]);
			
		// 	echo '</div>';

		// echo '</div>';

	?>
<?php endwhile; ?>