<?php global $post; ?>

<?php while (have_posts()) : the_post(); ?>
	
	<?php _partial('_header') ?>
	
	<div class="secao secao_01 bg-white my-10">
		<div class="container">
			<div class="row d-flex justify-content-between">
				<?php 					
					$imagem = get_field('1_secao_imagem');					
					$args = [							
						'class' => 'img-fluid mx-auto d-block'
					];
					echo '<div class="col-xl-6">';		
						_p('img:acf-image', $imagem , $args);
					echo '</div>';
					
					_p('div', get_field('1_secao_conteudo'), 'col-xl-5 font-weight-light mt-6 mt-xl-0');

				?>			
			</div>
		</div>
	</div>

	<div class="secao secao_02 py-6 py-lg-10 bg-cover" <?php acf_thumbnail_bg('2_secao_background') ?>>
		<div class="container">
			<div class="row">
				<?php _p('div', get_field('2_secao_conteudo'), 'col-xl-5 font-weight-light text-white'); ?>
			</div>
		</div>
	</div>


	<div class="bg-white container">

		<div class="secao secao_03 py-4 py-md-8">
			<?php _p('div', get_field('3_secao_conteudo'), 'shadow py-2 py-md-6 px-3 px-md-8 font-weight-light rubik'); ?>			
		</div>
		
		<?php $galeria = get_field('3_secao_galeria'); ?>
		<?php if ($galeria) :  ?>	
			<div class="secao mb-6">
				<div class="row justify-content-center">
					<div class="col-md-10">
						<div class="galeria-wall slick-until-sm slick-dots--light">
							<?php 
								for ($i=0; $i < count($galeria); $i++) { $img = $galeria[$i];
									echo '<div style="background-image: url('.$img['url'].');"></div>';
								}							
							?>
						</div>
					</div>
				</div>
			</div>		
		<?php endif; ?>

		<?php 
			$parceiros = get_field('parceiros');
			if ($parceiros) {
				foreach ($parceiros as $item) {
					
					echo '<div class="secao secao_04 my-6">';
						_p('h2', $item['titulo'], 'text-uppercase line line--short text-gold font-weight-bold');
						_p('p', $item['descricao'], 'h4 font-weight-normal');

						if ($item['galeria']) {
							echo '<ul class="slick p-0 slick-until-sm slick-dots--light list-default my-4">';
								foreach ($item['galeria'] as $img ) {
									_p('img',' ', [ 
										'class' => 'img-fluid mx-auto',
										'attr' => [
											'src' => $img['url'],
											'alt' => $img['alt'],
										],
										'before' => '<li class="m-md-2">',
										'after' => '</li>'
									]);	
								}
							echo '</ul>';
						}						
					echo '</div>';

				}
			}
		?>

	</div>

	
<?php endwhile; ?>