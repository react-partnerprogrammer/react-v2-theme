<?php if( have_rows('slider_principal') ): ?>

    <section class="featured--home" role="banner">
        
        <div class="container">
            
            <div class="slick--home">
                
                <?php while ( have_rows('slider_principal') ) : the_row(); ?>
                    <?php 
                        $link = get_sub_field('link_slider'); 
                        $lightbox = get_sub_field('abrir_em_lightbox') ? ' data-fancybox ' : '';
                    ?>
                                        
                    <div class="d-flex align-items-center justify-content-center">

                        <?php if ($link): ?>
                            <a href="<?php echo $link['url'] ?>" <?php echo $lightbox; ?> class="d-flex align-items-center justify-content-center" title="<?php echo $link['link'] ?>" target="<?php echo $link['target'] ?>">
                        <?php endif; ?>

                            <div class="text-white text-center">
                                <?php $imageDesktop = get_sub_field('imagem_slider') ?>
                                <?php $imageMobile = get_sub_field('imagem_mobile') ?>
                                
                                <?php if ($imageDesktop && $imageMobile) : ?>
                                    <figure class="bg-cover d-none d-md-block" style="background-image:url(<?php echo $imageDesktop ?>);"></figure>                                
                                    <figure class="bg-cover d-md-none" style="background-image:url(<?php echo $imageMobile ?>);"></figure>                                                                
                                <?php elseif ($imageDesktop): ?>
                                    <figure class="bg-cover" style="background-image:url(<?php echo $imageDesktop ?>);"></figure>                                
                                <?php endif; ?>
                                <?php the_sub_field('conteudo'); ?>
                                
                            </div>

                        <?php if ($link) : ?>
                            </a>
                        <?php endif; ?>

                    </div>

                <?php endwhile; ?>

            </div>

        </div>

    </section>

  <?php endif; ?>


  <?php _loop('secoes'); ?>

