<?php global $post; ?>

	
	<?php _partial('_header') ?>
	
	<div class="container my-8">
		<div class="row downloads justify-content-center">
			
			<?php 
				$extraStyles = '';				
				$cpts = new WP_Query( ['post_type' => 'portfolio', 'posts_per_page' => -1 ] ); 
				while ( $cpts->have_posts() ) : $cpts->the_post(); 
					$mobile = get_field('background_mobile');
					$desktop = get_field('background');
					if ($mobile && $desktop) {
						$extraStyles .= '#case-' . get_the_ID() . ' a:before { background-image: url('.$desktop.'); } ';
						$extraStyles .= '@media (max-width: 767px) { #case-' . get_the_ID() . ' a:before { background-image: url('.$mobile.'); }} ';
					} else {
						$extraStyles .= '#case-' . get_the_ID() . ' a:before { background-image: url('.$desktop.'); } ';
					}
					echo '<div id="case-'.get_the_ID().'" class="download bg-cover col-11 col-md-12 mb-2">';

						echo '<a class=" d-flex align-items-center justify-content-center flex-column " href="'.get_permalink().'">';
							_p('span', my_list_categories('areas'), 'headline rubik');
							_p('span', get_the_title(), 'titulo rubik h2 text-white font-weight-normal mb-0');								
						echo '</a>';

					echo '</div>';
				endwhile;
				wp_reset_postdata();
			?>
		</div>
	</div>

	<style type="text/css"> <?php echo $extraStyles; ?> </style>
	