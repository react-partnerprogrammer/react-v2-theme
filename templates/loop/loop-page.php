<?php global $post; ?>
<?php while (have_posts()) : the_post(); ?>
	
	<?php _partial('_header') ?>
	
	<div id="post-<?php the_ID(); ?>" <?php post_class('container'); ?> >
		<?php
			// Se não exister conteúdo, exibe as páginas filhas
			if (!$post->post_content) { echo do_shortcode('[paginas-filhas]'); }				
			the_content(); 
		?>
	</div>

<?php endwhile; ?>