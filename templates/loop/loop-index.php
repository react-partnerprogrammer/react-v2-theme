<?php 
	global $post;
	if (is_front_page()) {		
		_loop('front-page');
		
	}elseif (is_404()) {
		_loop('404');
		
	} elseif (is_single()) {
		_loop('single-' . $post->post_type);
		
	} elseif (is_page_template('templates/page-agencia.php')) {
		
		_loop('agencia');
		
	} elseif (is_page_template('templates/page-downloads-com-filtro.php')) {
		
		_loop('downloads-com-filtro');
		
	} elseif (
		is_page_template('templates/page-cases.php')
		|| is_archive() && is_post_type_archive('portfolio') 
		|| is_tax('areas')
	) {
		
		_loop('cases');
		
	} elseif ( 
		is_page_template('templates/page-servicos.php')
		|| is_archive() && is_post_type_archive('servico') ) {
		
		_loop('servicos');
		
	} elseif (is_page()) {
		_loop('page');
		
	} else {
		_loop('blog'); 
	}				
?>