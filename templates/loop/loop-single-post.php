<?php while (have_posts()) : the_post(); ?>
	
	<?php _partial('_header') ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class('container'); ?> >
	    
		<div class="conteudo col-lg-10 col-xl-8 mx-auto">
			<?php 
				global $post;
				$obj = get_post_type_object( $post->post_type );	
						
				// echo '<pre>'.print_r($post,1). '</pre>';
				// die();
				the_content();

				echo '<div class="my-6">'.do_shortcode('[easy-social-share]').'</div>';
				
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					// _partial('_comments-disqus');
					echo '<div class="section--comments">';
						comments_template();
					echo '</div>';
				}
				
			?>
		</div>

		<?php 
			_p('a', __(' < Voltar', 'react'), [ 
				'attr' => [ 
					'href' => get_permalink(get_option( 'page_for_posts' )),
					'class' => 'btn btn-gold rubik mx-auto text-white',
				],
			]);

			echo '<hr class="my-6" />';

			pp_related([
				'class' => 'secao--blog mb-8 mt-4 related--' . $post->post_type,
				'title' => __('Veja também', 'react'),
				'loop' => 'related-' . $post->post_type,
				'posts_per_page' => 3
			]);
		?>

	</div>
<?php endwhile; ?>