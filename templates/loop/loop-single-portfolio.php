<?php while (have_posts()) : the_post(); ?>
	
	<?php _partial('_header') ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class('container'); ?> >
	    
		<div class="col-lg-10 col-xl-8 mx-auto">
			<?php 
				global $post;
				$obj = get_post_type_object( $post->post_type );	
						
				// echo '<pre>'.print_r($post,1). '</pre>';
				// die();
				the_content();

				echo '<div class="my-6">'.do_shortcode('[easy-social-share]').'</div>';						

				echo '<hr class="my-6" />';
				
				$args = [
					'class' => 'mb-8 related--' . $post->post_type,
					'title' => __('Outros', 'react') . ' ' . $obj->label,
					'loop' => 'related-' . $post->post_type,
					'posts_per_page' => 2
				];
				
				echo  '<div id="post-relacionados" class="my-4 related '.$args['class'].'">',
							'<h4 class="h2 rubik font-weight-bold text-gold text-uppercase '.$args['titleClass'].'">'.(isset($args['title']) ? $args['title'] : __('Veja outros', 'react')).'</h4>',
							_p('p', $args['descricao'], [ 'class' => 'rubik h4 font-weight-normal mb-3 descricao', 'echo' => 'false']),
							'<div class="row">';          
								global $post;								
								$next_post = get_next_post();
								$prev_post = get_previous_post();
								// die();
								if ($next_post && $next_post->ID != get_the_ID()) {									
									$post = $next_post;
									setup_postdata( $post );
									echo '<article class="col-xl-6 mt-2 prev-post">';
										echo '<a href="'.get_permalink().'" '.get_acf_thumbnail_bg('background').' class="bg-cover rubik d-flex flex-column justify-content-center" title="'.get_the_title().'">';
											_p('span', my_list_categories('areas'), 'h6 text-white');
											_p('span', get_the_title(), 'text-gold h6 mb-0');
										echo '</a>';
									echo '</article>';									
								}			
																
								if ($prev_post && $prev_post->ID != get_the_ID()) {
									$post = $prev_post;
									setup_postdata( $post );									
									echo '<article class="col-xl-6 mt-2 next-post">';
										echo '<a href="'.get_permalink().'" '.get_acf_thumbnail_bg('background').' class="bg-cover rubik d-flex flex-column justify-content-center" title="'.get_the_title().'">';
											_p('span', my_list_categories('areas'), 'h6 text-white');
											_p('span', get_the_title(), 'text-gold h6 mb-0');
										echo '</a>';
									echo '</article>';
								}
								wp_reset_postdata();


				echo      '</div>',
						'</div>';			  
			?>
		</div>

	</div>
<?php endwhile; ?>