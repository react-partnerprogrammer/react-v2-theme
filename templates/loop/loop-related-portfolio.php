<?php 
    global $post;
    echo '<article class="col-xl-6 mt-2">';
        echo '<a href="'.get_permalink().'" '.get_acf_thumbnail_bg('background').' class="bg-cover rubik d-flex flex-column justify-content-center" title="'.get_the_title().'">';
            _p('span', my_list_categories('areas'), 'h6 text-white');
            _p('span', get_the_title(), 'text-gold h6 mb-0');
        echo '</a>';
    echo '</article>';
?>