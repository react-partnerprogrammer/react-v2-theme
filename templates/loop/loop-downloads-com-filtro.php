<?php global $post; ?>

<?php while (have_posts()) : the_post(); ?>
	
	<?php _partial('_header') ?>

	<div class="container my-8">
		<div class="row downloads justify-content-center">
			<?php 
				$count = 0; $extraStyles = '';
				while ( have_rows('downloads') ) : the_row();
					
					$link = get_sub_field('link');
					$link_juicer = get_sub_field('link_juicer');
					$juicer_class = $link_juicer ? ' ljoptimizer ' : '';
					// echo get_sub_field('background');
					// echo '<pre>'.print_r($link,1). '</pre>';
					$extraStyles .= '.download-' . $count . ' > *:before { background-image: url('.get_sub_field('background').'); } ';

					echo '<div class="download download-'. $count .' bg-cover col-11 col-md-6 mb-2 mb-md-0">';

						echo '<a class="d-flex align-items-center '.$juicer_class.' justify-content-center flex-column " href="'.$link['url'].'" target="'.$link['target'].'" title="Download">';
							_p('span', get_sub_field('headline'), 'headline rubik');
							_p('span', get_sub_field('titulo'), 'titulo rubik h2 text-white font-weight-normal');
							_p('span', get_sub_field('descricao'), 'descricao text-white open-sans');
							_p('span', $link['title'], 'btn btn-primary btn-lg d-inline-block mt-2 px-4 text-white');
						echo '</a>';

					echo '</div>';
					
					$count++;
				endwhile;
			?>
		</div>
	</div>

	<style type="text/css">
		<?php echo $extraStyles; ?>
	</style>
<?php endwhile; ?>