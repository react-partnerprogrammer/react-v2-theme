<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">		
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?> id="top">
				
		<header id="header">			
			<div class="container">
				<?php _partial('_menu-nav'); ?>					
			</div>
			<div id="header--line"></div>
		</header>



