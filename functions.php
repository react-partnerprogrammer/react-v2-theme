<?php

// Define theme constants
define('PP_PARTIAL_PATH', get_stylesheet_directory() . '/templates/partials/');
define('PP_LOOP_PATH', get_stylesheet_directory() . '/templates/loop/');

// Setup Framework Base Functions
require_once ('functions/activation.php'); // Functions to run after install theme
require_once ('functions/setup.php');

// Filters, Hooks and functions about wp-admin
require_once ('functions/wp-admin.php');

// Wordpress Bootrap Navwalker
require_once ('functions/vendor/wp_bootstrap_navwalker.php');

// Custom functions for theming
require_once ('functions/custom-actions.php');
require_once ('functions/custom-shortcakes.php');
require_once ('functions/custom-functions.php');
require_once ('functions/custom-acf.php');
require_once ('functions/custom-wpcf7.php');



function cptui_register_my_cpts() {

	/**
	 * Post Type: Cases.
	 */

	$labels = array(
		"name" => __( "Cases", "pp" ),
		"singular_name" => __( "Case", "pp" ),
    );
            
	$args = array(
		"label" => __( "Cases", "pp" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => __( "cases", "pp" ), "with_front" => false ),
		"query_var" => true,
		"menu_position" => 3,
		"menu_icon" => "dashicons-images-alt2",
		"supports" => array( "title", "editor", "thumbnail" ),
		"taxonomies" => array( "areas" ),
	);

	register_post_type( "portfolio", $args );

	/**
	 * Post Type: Soluções.
	 */

	$labels = array(
		"name" => __( "Soluções", "pp" ),
		"singular_name" => __( "Solução", "pp" ),
	);    
    
	$args = array(
		"label" => __( "Soluções", "pp" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => __( "solucoes", "pp" ), "with_front" => false ),
		"query_var" => true,
		"menu_position" => 2,
		"menu_icon" => "dashicons-megaphone",
		"supports" => array( "title", "editor" ),
	);

	register_post_type( "servico", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );


add_filter('wpseo_title', 'filter_product_wpseo_title');
function filter_product_wpseo_title($title) {
    if(  !is_admin() && is_archive() && is_post_type_archive('servico')) {
		$pageID = function_exists('icl_object_id') ? icl_object_id(10, 'page', false, ICL_LANGUAGE_CODE) : 10;
		$title = get_post_meta($pageID, '_yoast_wpseo_title', true);		
		return $title;
    } elseif ( !is_admin() && is_archive() && is_post_type_archive('portfolio')) {
		$pageID = function_exists('icl_object_id') ? icl_object_id(8, 'page', false, ICL_LANGUAGE_CODE) : 8;	
		$title = get_post_meta($pageID, '_yoast_wpseo_title', true);		
		return $title;
	} else {
		return $title;
	}    
}

add_filter('wpseo_metadesc', 'filter_product_wpseo_description');
function filter_product_wpseo_description($title) {
    if( !is_admin() && is_archive() && is_post_type_archive('servico')) {
		$pageID = function_exists('icl_object_id') ? icl_object_id(10, 'page', false, ICL_LANGUAGE_CODE) : 10;
		$title = get_post_meta($pageID, '_yoast_wpseo_metadesc', true);		
		return $title;
    } elseif ( !is_admin() && is_archive() && is_post_type_archive('portfolio')) {
		$pageID = function_exists('icl_object_id') ? icl_object_id(8, 'page', false, ICL_LANGUAGE_CODE) : 8;
		$title = get_post_meta($pageID, '_yoast_wpseo_metadesc', true);		
		return $title;
	} else {
		return $title;
	}    
}

/* ---------------------------------------------------------------------------
 * Set hreflang="x-default" with WPML
 * --------------------------------------------------------------------------- */
// add_filter('wpml_alternate_hreflang', 'wps_head_hreflang_xdefault', 10, 2);
function wps_head_hreflang_xdefault($url, $lang_code) {
      
    if($lang_code == apply_filters('wpml_default_language', NULL )) {
          
        echo '<link rel="alternate" href="' . $url . '" hreflang="x-default" />'.PHP_EOL;
    }
      
    return $url;
}


add_filter('apto/navigation_sort_apply', 'theme_apto_navigation_sort_apply');
function theme_apto_navigation_sort_apply($current) {
		global $post;
		
		if($post->post_type == 'portfolio')
				$current    =   TRUE;
				else
				$current    =   FALSE;
		
		return $current;   
}
		
// function my_previous_post_where() {
// 	global $post, $wpdb;
// 	return $wpdb->prepare( "WHERE p.menu_order < %s AND p.post_type = %s AND p.post_status = 'publish'", $post->menu_order, $post->post_type);
// }
// add_filter( 'get_previous_post_where', 'my_previous_post_where' );
// function my_next_post_where() {
// 	global $post, $wpdb;
// 	return $wpdb->prepare( "WHERE p.menu_order > %s AND p.post_type = %s AND p.post_status = 'publish'", $post->menu_order, $post->post_type);
// }
// add_filter( 'get_next_post_where', 'my_next_post_where' );
// function my_previous_post_sort() {
// 	return "ORDER BY p.menu_order desc LIMIT 1";
// }
// add_filter( 'get_previous_post_sort', 'my_previous_post_sort' );
// function my_next_post_sort() {
// 	return "ORDER BY p.menu_order asc LIMIT 1";
// }
// add_filter( 'get_next_post_sort', 'my_next_post_sort' );



function custom_comments($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);

  if ( 'div' == $args['style'] ) {
    $tag = 'div';
    $add_below = 'comment';
  } else {
    $tag = 'li';
    $add_below = 'div-comment';
  }
?>
  <<?php echo $tag ?>
  <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?>>
    <?php if ( 'div' != $args['style'] ) : ?>
      <div id="comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <?php if ( $comment->comment_approved == '0' ) : ?>
      <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em><br />
    <?php endif; ?>
    <div class="comment-meta">
			<?php echo '<h4 class="name">'. ucwords($comment->comment_author) .'</h4>'; ?>			
    </div>
			<div class="comment-body">
				<?php			
					comment_text();
					if (is_user_logged_in()) {
						comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );           
					}
				?>
			</div>
  <?php if ( 'div' != $args['style'] ) : ?>
  </div>
  <?php endif; ?>
<?php
}
