<form id="search-form" role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label class="sr-only"> Buscar no site </label>
	<div class="input-group add-on">		
		<input type="search" id="search-el" class="search-field form-control" placeholder="<?php _e('O que você procura?', 'react') ?>" value="<?php echo get_search_query() ?>" required name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
		<button type="submit"></button>    
	</div>
</form>