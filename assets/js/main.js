
// Initiate the ScrollMagic Instance
const controller = new ScrollMagic.Controller();
const secoes = [...document.getElementsByClassName('secao')];
const parallaxBanners = [...document.getElementsByClassName('cta_banner_parallax')];
const lines = [...document.getElementsByClassName('line')];

// Add swipe-in-fade to all sections
secoes.map( (secao) => {
    new ScrollMagic.Scene({
        triggerElement: secao,
        triggerHook: 0.8,
        reverse: false,
    })
    .setTween(secao, 2, {
        autoAlpha: 1,
        ease: Expo.easeOut,
        className: '+=start',
        y: '0',
    })
    // .addIndicators({name: 'Seção'})
    .addTo(controller);
});

// Add Parallax event to each banner
parallaxBanners.map( (secao) => {
    // console.log(secao.getElementsByClassName('banner').children[0]);
    new ScrollMagic.Scene({
        triggerElement: secao,
        triggerHook: 0.7,
        duration: '100%',
    })
    .setTween($(secao).find('a'), {
        ease: Linear.easeNone,
        y: '-3%',
    })
    // .addIndicators({name: 'Parallax Imagem'})
    .addTo(controller);

    new ScrollMagic.Scene({
        triggerElement: secao,
        triggerHook: 0.7,
        duration: '80%',
    })
    .setTween($(secao).find('.col-lg-8'), {
        ease: Linear.easeNone,
        y: '-5%',
    })
    // .addIndicators({name: 'Parallax Conteudo'})
    .addTo(controller);
});

lines.map( (line) => {
    new ScrollMagic.Scene({
        triggerElement: line,
        triggerHook: 0.65,
        reverse: false,
    })
    .setTween(line, 2, {
        className: '+=line--start',
    })
    // .addIndicators({name: 'Line'})
    .addTo(controller);
});

// Astronaut background animation
if ($('#astronauta-right').length) {
    new ScrollMagic.Scene({
        triggerElement: '#astronauta-right',
        offset: -100,
        triggerHook: .8,
        // reverse: false,
        duration: '150%',
    })
    .setTween('#astronauta-right', {ease: Linear.easeNone, y: '+=30'})
    // .addIndicators({name: 'Astronauta'})
    .addTo(controller);
}

// Astronaut background animation
if ($('#astronauta-left').length) {
    new ScrollMagic.Scene({
        triggerElement: '#astronauta-left',
        triggerHook: 1,
        reverse: true,
        duration: '100%',
    })
    .setTween('#astronauta-left', {ease: Linear.easeNone, y: '+=90'})
    // .addIndicators({name: 'Astronauta'})
    .addTo(controller);
}

// Satelite background animation
if ($('#satelite').length) {
    new ScrollMagic.Scene({
        triggerElement: '#satelite',
        offset: -100,
        triggerHook: .8,
        // reverse: true,
        duration: '100%',
    })
    .setTween('#satelite', {ease: Linear.easeNone, y: '-=30'})
    // .addIndicators({name: 'Satelite'})
    .addTo(controller);
}

if ($('.pageheader').length) {
    new ScrollMagic.Scene({
        triggerElement: '#main-content',
        triggerHook: 0,
        reverse: true,
        duration: $('.pageheader').outerHeight() / 2,
    })
    .setTween('.pageheader .container', {
        autoAlpha: 0,
    })
    // .addIndicators({name: 'Header'})
    .addTo(controller);
}

/** Slicks **/
$('.slick--carousel').each(function() {
    let appendArrowsEl = $(this).hasClass('arrows--title') ? $(this).closest('.container').find('.append-arrows .arrows') : $(this);
    let settingsMd = $(this).hasClass('slick--carousel--testimonail') ?
        {slidesToShow: 2, slidesToScroll: 2}
         : {slidesToShow: 3, slidesToScroll: 3};
    let settingsLg = $(this).hasClass('slick--carousel--testimonail') ?
        {slidesToShow: 3, slidesToScroll: 3, arrows: true, dots: false}
         : {slidesToShow: 4, slidesToScroll: 4, arrows: true, dots: false};
    let settingsXl = $(this).hasClass('slick--carousel--testimonail') ?
        {slidesToShow: 3, slidesToScroll: 3, arrows: true, dots: false}
         : {slidesToShow: 5, slidesToScroll: 5, arrows: true, dots: false};

    $(this).slick({
        infinite: false,
        speed: 300,
        arrows: false,
        dots: true,
        rows: 0,
        appendArrows: appendArrowsEl,
        prevArrow: '<button type="button" class="slick-prev icon-slider-left">Previous</button>',
        nextArrow: '<button type="button" class="slick-next icon-slider-right">Next</button>',
        responsive: [
          {
            breakpoint: 9999,
            settings: settingsXl,
          },
          {
            breakpoint: 1250,
            settings: settingsLg,
          },
          {
            breakpoint: 992,
            settings: settingsMd,
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });
});

    $('.slick-until-sm').slick({
        dots: true,
        arrows: false,
        rows: 0,
        responsive: [
            {
              breakpoint: 9999,
              settings: 'unslick',
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
              },
            },
          ],
    });

    $('.slick--home').slick({
        dots: false,
        arrows: true,
        rows: 0,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-prev icon-slider-left">Previous</button>',
        nextArrow: '<button type="button" class="slick-next icon-slider-right">Next</button>',
    });

/** Slicks **/

/** Menu **/
    $('.navbar-fire').on('click', function() {
        $('body').toggleClass('menu--visible');
        $(this).toggleClass('collapsed');
    });
/** Menu **/

/** Scroll to Top **/

    $(window).scroll(function() {
        checkScrollToTop();
    });

    function checkScrollToTop() {
        // Se não for no single anula
        if ($('body').hasClass('home')) return;
        let el = $('#go-to-top');
        let alturaWindow = $(window).height();
        let alturaScrolled = $(document).scrollTop();

        // Se já tiver sido rolado + de 1 + 50px da tela, mostra o #gotoTop
        if (alturaScrolled > (alturaWindow / 2 )) {
           el.addClass('active');
        } else {
            el.removeClass('active');
        }
    }
    function suffle() {
    }
/** Scroll to Top **/


/** After Expand **/
    if ($(window).width() >= 992) {
        // Allow click at dropdown menus
        $('#navbarNav .dropdown-toggle').click(function() {
            window.location = $(this).attr('href');
        });

        // Sticky menu
        let stickerMenu = new ScrollMagic.Scene({
            triggerElement: 'header#header',
            triggerHook: 0,
        })
        .setPin('header#header')
        // .addIndicators('Sticky Menu')
        .addTo(controller);

        $(window).on('resize', function() {
            // console.log('Atualizando controller');
            controller.updateScene(stickerMenu, true);
            controller.update(true);
        });

        // Add Background to Sticky Menu
        new ScrollMagic.Scene({
            triggerElement: '.pageheader',
            triggerHook: 0,
            offset: $('.pageheader').outerHeight() - $('#navmenu').outerHeight() - 3,
        })
        .setTween('header#header', .3, {
            className: '+=header--fixed',
        })
        // .addIndicators('Sticky Menu Background')
        .addTo(controller);

        // Control the width from gold bar header
        new ScrollMagic.Scene({
            triggerElement: '.pageheader',
            triggerHook: 0,
            offset: $('.pageheader').outerHeight() - $('#navmenu').outerHeight() - 3,
            duration: $('#main-content').outerHeight() - $('.pageheader').outerHeight(),
            reverse: true,
        })
        // .setTween(TweenLite.to(yellowBar, $('main-content').outerHeight(), {cssRule: {width: '100%'}}))
        .setTween('#header--line', {width: '100%'})
        // .addIndicators('Sticky Menu Line Width')
        .addTo(controller);
    /** After Expand **/
    }

/** Mouse Moving **/
    $(document).mousemove(function(event) {
        $('.mousemove').each(function(index, element) {
            let base = $(this).data('speed');
            let direction = $(this).data('direction') ? $(this).data('direction') : 1;
            let xPos = (event.clientX/$(window).width())-0.5;
            let yPos = (event.clientY/$(window).height())-0.5;
            let box = element;

            TweenLite.to(box, 1, {
                x: xPos * base * direction,
                y: yPos * base * direction,
                ease: Power1.easeOut,
            });
        });
    });
/** Mouse Moving **/

// $('#controls button').click(function(e) {
//     e.preventDefault();
//     let target = $(this).data('target');
//     $(target).toggleClass('sumir');
// });
// $('#controls button:last-of-type').click();
