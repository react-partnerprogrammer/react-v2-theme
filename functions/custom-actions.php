<?php
// Default actions for PP Framework

# 01 - Popups on footer
add_action( 'wp_footer', 'pp_popups');



// You can add popups in front page
function pp_popups() {
  global $post, $popupContent, $cssAdicional;
  
  // Prevent erros in page without $post (Ex: 404 error)
  if (!$post) return;
  
  // Get popups
  $popups = get_field( 'pp-popups', 'options' );
    
  if ($popups) {     
    foreach ($popups as $popup) {
      // If popup are active and is to current page 
      if ($popup['ativo'] && in_array($post->ID, $popup['localizacao'])) {
        $popupContent = $popup['conteudo'][0]; $cssAdicional = $popup['css_adicional'];
        // Chama o arquivo baseado no layout
        include(PP_BLOCK_PATH . '_popups-'. $popupContent['acf_fc_layout']. '.php');
      }
    }
  }
}


function my_acf_save_post_content( $post_id ){
  
	if(get_post_type($post_id) == 'servico'){ //just for good measure so it doesn't run on unnecessery posttypes
		// vars		
	 
    // Update post
    $my_post = array(
      'ID'           => $post_id,
      // 'post_content' => 'teste'
      'post_content' => get_field('conteudo', $post_id)
    );
			// Update the post into the database
			remove_action('acf/save_post', 'my_acf_save_post_content', 20);
			  wp_update_post( $my_post );
      add_action('acf/save_post', 'my_acf_save_post_content', 20);
      
	}
}
 
// run after ACF saves the $_POST['fields'] data
add_action('acf/save_post', 'my_acf_save_post_content', 20);

// Change pagination for blogs
function my_pagination_for_archives( $query ) {
  
  if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'servico' ) ) {  
      $query->set( 'posts_per_page', -1 );
  }
  
  if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'portfolio' ) ) {  
      $query->set( 'posts_per_page', -1 );
  }

}
add_action( 'pre_get_posts', 'my_pagination_for_archives' );

