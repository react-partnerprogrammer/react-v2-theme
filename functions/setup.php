<?php


/* Load Javascripts */
/* ----------------------------------------- */

function pp_load_scripts(){
 	
 	$path_js = get_template_directory_uri() . '/assets/js/';
 	$path_css = get_template_directory_uri() . '/assets/css/';
  	
            		
	wp_deregister_script('jquery');	
	wp_dequeue_script('jquery');
	wp_deregister_script( 'wp-embed' );

	wp_enqueue_script('jquery', '//cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js', [], false, true);
	wp_enqueue_script('gsap', '//cdn.jsdelivr.net/npm/gsap@2.0.2/umd/TweenMax.min.js', ['jquery'], false, true);		
	// wp_enqueue_script('fancybox', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js', ['jquery'], false, true);		
	// wp_enqueue_script('gsap-css-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSPlugin.min.js', [], false, true);				
	// wp_enqueue_script('gsap-css-rule-plugin', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSRulePlugin.min.js', [], false, true);				
	wp_enqueue_script('scrollmagic', '//cdn.jsdelivr.net/npm/scrollmagic@2.0.5/scrollmagic/minified/ScrollMagic.min.js', [], false, true);				
	wp_enqueue_script('scrollmagic--gsap', '//cdn.jsdelivr.net/npm/scrollmagic@2.0.5/scrollmagic/minified/plugins/animation.gsap.min.js', [], false, true);
	// wp_enqueue_script('scrollmagic--debug', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', ['scrollmagic'], false, true);				
	// wp_enqueue_script('flickty', '//unpkg.com/flickity@2/dist/flickity.pkgd.min.js', [], false, true);				
	wp_enqueue_script('lazyload', '//cdn.jsdelivr.net/npm/vanilla-lazyload@12.4.0/dist/lazyload.min.js', [], false, true);				
	wp_enqueue_script('slickjs', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', ['jquery'], false, true);				
	wp_enqueue_script('bootstrap-js','//cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js', ['jquery'], false, true);
	wp_enqueue_script('pp-main', $path_js . 'main-min.js', ['jquery','slick', 'gsap', 'scrollmagic'], false, true);
	// wp_enqueue_script('pp-libs', $path_js . 'libs.js', [], false, true);
				
	// wp_localize_script( 'jquery', 'siteVars', [] );
	
	// wp_enqueue_style( 'libs', $path_css . 'libs.css');
	wp_enqueue_style( 'bootstrap', $path_css . 'bootstrap.css');
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?display=swap&family=Rubik:300,400,500,700,900|Open+Sans:400|Lora:400,400i,700,700i');
	wp_enqueue_style( 'main', $path_css . 'main.css');
	// wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
	// wp_enqueue_style( 'fancybox', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css');
	
	// on single blog post pages with comments open and threaded comments
	if ( is_singular() ) { 
		// enqueue the javascript that performs in-link comment reply fanciness
		wp_enqueue_script( 'comment-reply' ); 
	}

	if (get_page_template_slug() != 'templates/page-contato.php') {
		wp_dequeue_script('contact-form-7'); # Restrict scripts.
		wp_dequeue_script('google-recaptcha');
		wp_dequeue_style('contact-form-7'); # Restrict css.
	}
	
}
add_action( 'wp_enqueue_scripts', 'pp_load_scripts' );


add_action( 'wp_print_styles', 'my_deregister_styles', 100 );

function my_deregister_styles() {
	if (!is_user_logged_in()) {
		 wp_deregister_style( 'dashicons' );		
	}
}

// add_action( 'wp_enqueue_scripts', 'crunchify_print_scripts_styles');
function crunchify_print_scripts_styles() {

	$result = [];
	$result['scripts'] = [];
	$result['styles'] = [];

	// Print all loaded Scripts
	global $wp_scripts;
	foreach( $wp_scripts->queue as $script ) :
		 $result['scripts'][] =  $wp_scripts->registered[$script]->src . ";";
	endforeach;

	// Print all loaded Styles (CSS)
	global $wp_styles;
	foreach( $wp_styles->queue as $style ) :
		 $result['styles'][] =  $wp_styles->registered[$style]->src . ";";
	endforeach;

	echo '<pre>'.print_r($result,1). '</pre>';
	die();
	return $result;
}


function pp_load_admin_scripts() {

	$path_js = get_template_directory_uri() . '/assets/js/';
	$path_css = get_template_directory_uri() . '/assets/css/';
	 
	wp_enqueue_style( 'custom-admin-style', $path_css. 'admin-style.css');	
}
add_action('admin_enqueue_scripts', 'pp_load_admin_scripts');

// Runs a function after_setup_theme
add_action( 'after_setup_theme', 'pp_setup' );

if ( ! function_exists( 'pp_setup' ) ):
	
	// Configura os padrões do tema e suporte a algumas funções do wp.
	function pp_setup() {
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

		// Add callback for custom TinyMCE editor stylesheets.
		add_editor_style('assets/css/editor-style.css');

		// Registers theme menu's 
		register_nav_menus( array(
			'primary' => __( 'Navegação Global', 'pp' ),
			'secondary' => __( 'Navegação Local', 'pp' ),
		) );

}
endif;


// Adiciona tamanho de thumbs customizáveis
add_action('init', 'add_custom_image_sizes');

function add_custom_image_sizes() {
	add_image_size('post-gallery', 750, 440, true); 
	// add_image_size('slider-destaque', 1170, 350, true);
	// add_image_size('imagem-thumb', 800, 600, true);
}


/* Excerpt */
/* ----------------------------------------- */
	
	function twentyten_excerpt_length( $length ) { return 40; }
	add_filter( 'excerpt_length', 'twentyten_excerpt_length' );

	function twentyten_continue_reading_link() {
		return '';
		// return ' <a href="'. get_permalink() . '" title="Veja mais sobre '. get_the_title() .'">' . __( 'Saiba mais', 'pp' ) . '</a>';
	}

	function twentyten_auto_excerpt_more( $more ) {
		return ' &hellip;' . twentyten_continue_reading_link();
	}
	add_filter( 'excerpt_more', 'twentyten_auto_excerpt_more' );


	function twentyten_custom_excerpt_more( $output ) {
		if ( has_excerpt() && ! is_attachment() ) {
			$output .= twentyten_continue_reading_link();
		}
		return $output;
	}
	add_filter( 'get_the_excerpt', 'twentyten_custom_excerpt_more' );
/* ----------------------------------------- Excerpt */		



/**
 * Register widgetized areas 
 */
function twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Sidebar', 'pp' ),
		'id' => 'sidebar-principal',
		'description' => __( 'Arraste os itens desejados até aqui. ', 'pp' ),
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="sidebar-title">',
		'after_title' => '</h3>',
	) );

}

/** Register sidebars by running twentyten_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'twentyten_widgets_init' );


	

/* 
	Filtro para criar container responsivo nos embeds do the_content
	Style no @pp-default-styles.less
/* ----------------------------------------- */
	add_filter('embed_oembed_html', 'wrap_embed_with_div', 10, 3);
	function wrap_embed_with_div($html, $url, $attr) {
	        return "<div class=\"responsive-container\">".$html."</div>";
	}
/* ----------------------------------------- Filtro para criar container responsivo nos embeds do the_content */		
