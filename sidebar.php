<aside id="sidebar" class="sidebar col-lg-3">
	<?php 
		echo '<div class="d-none d-md-block">';
			get_search_form();
		echo '</div>';
    	if ( is_active_sidebar( 'sidebar-principal' ) ) :
    	   dynamic_sidebar( 'sidebar-principal' );
    	endif;
    ?>
</aside>